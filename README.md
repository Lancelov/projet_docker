****Authors:****
MUSCIO Aurélien
LANCELOT Valentin
LAMACCHIA Dylan


### Fichier descriptif sur la mise en place d'un conteneur visant à utiliser les services DVWA, Mysql et PhpMyAdmin.

****Pré-requis:****
- Disposer d'un environnement de travail sous un système d'exploitation Linux
- Installer le package Docker permettant l'utilisation de conteneurs
- Avoir un compte administrateur pour éxécuter les commandes "Docker"
- Installer un navigateur web pour accéder à l'application DVWA

****1-Compiler et lancer le conteneur:****
    - compilation: docker-compose build
    - éxecution du docker: docker-compose up
    
****2-Paramétrer les variables d'environnements:****
Il faut renommer le fichier ".env.sample" en ".env" (mv .env.sample .env) puis modifier les variables ci-dessous comme souhaité:
# 
    - PORT_DVWA => Utiliser pour accéder à l'interface web de DVWA
    - PORT_PHPMYADMIN => Utiliser pour accéder a phpmyadmin
    - MYSQL_ROOT_PASSWORD => Utiliser par DVWA pour la connexion à la base de donnée MYSQL
    - MYSQL_PASSWORD => Password MYSQL de l'utilisateur 
    - MYSQL_DATABASE => Nom de la base de donnée
    - MYSQL_USER =>Login MYSQL de l'utilisateur
# 
***PS:*** Il faut modifier les valeurs des variables suivantes dans le fichier Docker/config.inc.php et les faire correspondre:
# 
    $_DVWA[ 'db_database' ] = '';
    $_DVWA[ 'db_user' ]     = '';
    $_DVWA[ 'db_password' ] = '';
#  
****3-Création/Utilisation de la base de données:****

Il faut ensuite cliquer sur Create/ Reset Database et entrer les identifiants par défaut de l'application DVWA:
```
login = admin
password = password 
```
- ./database:/var/lib/mysql => volume nommé utilisé pour stocker les fichiers de la base de données mySql

****4-Utiliser le service DVWA:****

Pour se connecter à l'interface de DVWA, il suffit d'ouvrir un navigateur et d'entrer l'URL suivante:
http://localhost:PORT_DVWA
Vous pouvez désormais utiliser librement l'application DVWA depuis le conteneur.
- /var/www/html => volume nommé utilisé pour stocker les fichiers de configuration de DVWA
    
****5-Utiliser le service phpMyadmin:****

Afin d'acceder au service PhpMyAdmin il faut ouvrir un navigateur web et rentrer l'URL suivant: http://localhost:PORT_PHPMYADMIN
Les identifiants pour se connecter à l'interface de phpMyAdmin sont à définir dans le .env (MYQSL_USER et MYSQL_PASSWORD).







